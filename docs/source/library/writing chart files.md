# Writing charts

Similar to how you can read a chart file with a loader, writing a chart file
is done by using a *dumper*, it's a function that takes in a [`Song`](Song)
object and an output path template and return a dict that maps path names to a
[`bytes`](bytes) objects with the file contents.

Let's go over how you can import then use one.

## Importing a dumper

Much like loaders, dumpers are all defined somewhere in the `jubeatools.formats`
module.

The precise import path of each dumper is documented here :
[](<../api/loaders and dumpers.md>)

For example you can import the dumper for `#memo2` files like this :

```python
from jubeatools.formats.jubeat_analyser import dump_memo2
```

:::{tip}
Just like for loaders, if you don't want to have to write down the full import
path or if you don't know which formats you will have to write to in advance,
you can import [`DUMPERS`](DUMPERS) from `jubeatools.formats` to query the
correct dumper based on the format. It's a dict that maps [`Format`](Format)
enum members to their associated dumper.

```python
>>> from jubeatools.formats import DUMPERS, Format
>>> dump_memo2 = DUMPERS[Format.MEMO_2]
```
:::

## Using a dumper

Dumpers take in a [`Song`](Song) object and an output path template

```python
song = Song(...)
path = Path("new_file")
files = dumper(song, path)
```

Some dumpers also accept extra options as keyword arguments

```python
files = dumper(song, Path("new_file"), make_it_pretty=True)
```

These extra options are specific to each dumper and are documented
in [](<../api/loaders and dumpers.md>).

## The output path template

Dumpers all take in an output path template. If it doesn't point to an existing
*folder*, the path is used as a [format string](https://docs.python.org/3/library/string.html#format-string-syntax).

The following parameters are available for the format string, they are all
passed as `str` objects :

| name                | description                                        |
|---------------------|----------------------------------------------------|
| `title`             | song title                                         |
| `difficulty`        | uppercase BSC ADV EXT                              |
| `difficulty_index`  | 0-based difficulty index, (BSC: 0, ADV: 1, EXT: 2) |
| `difficulty_number` | 1-based                                            |
| `dedup`             | dedup string ("-1", "-2" etc ...)                  |

For example `"{title} {difficulty}.txt"` would generate filenames like
`"SigSig BSC.txt"`, `"SigSig ADV.txt"`, or `"SigSig EXT.txt"`

jubeatools adds support for suffix `u` and `l` in the format specification
string for uppercase and lowercase respectively. This means that for example
`"{difficulty:l}.eve"` would generate filenames like `"bsc.eve"`, `"adv.eve"`
or `"ext.eve"`.

If a generated filename points to a file that already exists, a deduplicator
will be added right before the extension : `"SigSig EXT-1.txt"`,
`"SigSig EXT-2.txt"`, `"SigSig EXT-3.txt"` etc ...

If the output path template points to an existing folder, the dumper will
generate paths to files inside that folder. The filenames will follow a
template preset that tries to mimick what's usual for files in the format the
dumper outputs. These preset templates are documented in
[](<../api/loaders and dumpers.md>).

## The Dumper Protocol

Dumpers have a *uniform* interface, in other words they are all compatible with
the same function signature. It's the Dumper *Protocol* :

```{py:function} dump(song: Song, path: pathlib.Path, **kwargs: Any) -> Dict[pathlib.Path, bytes]

Convert the contents of `song` to files with associated name suggestions.

Possibly takes in some options via the kwargs.

:param Song song: Song object to be exported
:param pathlib.Path path: output path template
:param Any **kwargs: Format-specific options
:return: A dict that maps filenames to file contents as bytes
:rtype: Dict[pathlib.Path, bytes]
```