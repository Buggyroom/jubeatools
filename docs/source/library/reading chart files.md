# Reading charts

Reading a chart file is done by using a *loader*, it's a function that takes in
a path and returns a [`Song`](Song) object.

Let's go over how you can import then use one.

## Guessing the format

If you need to read chart files whose format you don't know in advance, you can
use the [`guess_format()`](guess_format) function. It's the same function the
CLI uses under the hood when you don't specify the input format yourself.

```python
>>> from pathlib import Path
>>> from jubeatools.formats.guess import guess_format
>>> guess_format(Path("sigsig.txt"))
<Format.MEMO_2: 'memo2'>
```

```{warning}
[`guess_format()`](guess_format) makes an honest attempt at guessing but it
doesn't work for all files 100% of the time. If you *know* that you will only
ever read a single format, I **strongly** recommend you import the correct
loader directly
```

## Importing a loader

Loaders are all defined somewhere in the `jubeatools.formats` module. The
precise import path of each loader is documented here :
[](<../api/loaders and dumpers.md>)

For example you can import the loader for `#memo2` files like this :

```python
from jubeatools.formats.jubeat_analyser import load_memo2
```

:::{tip}
If you don't want to have to write down the full import path or if you don't
know which format you will have to read in advance, you can import
[`LOADERS`](LOADERS) from `jubeatools.formats` to query the correct loader
based on the format. It's a dict that maps [`Format`](Format) enum members to
their associated loader.

```python
>>> from jubeatools.formats import LOADERS, Format
>>> load_memo2 = LOADERS[Format.MEMO_2]
```
:::

## Using a loader

Once you've imported or queried your loader, you can use it as follows :

```python
from pathlib import Path
path = Path("my_file.txt")
song = load_thing(path)
```

:::{note}
Some loaders accept extra options as keyword arguments :

```python
song = load_thing(path, splines="reticulate")
```

These extra options are specific to each loader and are documented in
[](<../api/loaders and dumpers.md>).
:::

Check out [](<song object.md>) to see how the chart data is organized in a
[`Song`](Song) object.

## The Loader Protocol

Loaders have a *uniform* interface, in other words they are all compatible with
the same function signature. It's the Loader *Protocol* :

```{py:function} load(path: pathlib.Path, **kwargs: Any) -> Song

Read what's in `path` and turn it into a `Song` object.

Possibly takes in some options via the kwargs.

:param pathlib.Path path: path to a file or folder to be read
:param Any **kwargs: Format-specific options
:return: the Song instance read from the file(s)
:rtype: Song
```