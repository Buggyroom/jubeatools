# jubeatools

A toolbox for jubeat file formats

## For Charters

If you just want to use jubeatools to convert a chart, read these pages :

```{toctree}
:caption: For Charters
:maxdepth: 1
how to install jubeatools
how to convert charts
```

## For Developers

```{toctree}
:caption: For Developers
:maxdepth: 1
library/index
cli
api/index
changelog.md
```

