# Command-line Interface

## Usage

```console
$ jubeatools [OPTIONS] SRC DST
```

## Positional Arguments

`SRC`
: *source*, path to the input chart file or chart folder

`DST`
: *destination*,  output path template to use for the output file or set of
  files. See [](<library/writing chart files.md#the-output-path-template>) for more
  info

## Options

### Input

`--input-format=FORMAT`
: Force jubeatools to read the input file / folder as the given format. If this
  option is not used jubeatools will try to guess the format using
  [`guess_format()`](guess_format).

  Possible values : `eve`, `jbsq`, `malody`, `memon:legacy`, `memon:v0.1.0`,
  `memon:v0.2.0`, `memon:v0.3.0`, `memon:v1.0.0`, `mono-column`, `memo`,
  `memo1`, `memo2`, `yubiosi:v1.0`, `yubiosi:v1.5`, `yubiosi:v2.0`

`--beat-snap=NUMBER`
: Snap all notes and bpm changes to the nearest {math}`\frac{1}{\texttt{NUMBER}}` beat

  Applies to the following formats : `eve`, `jbsq`

  Possibles values : Integers >= 1

`--merge`
: If called on a folder, merge all the chart files found inside with a
  "permissive" strategy.

  Applies to the following formats: `memon:legacy`, `memon:v0.1.0`,
  `memon:v0.2.0`, `memon:v0.3.0`, `memon:v1.0.0`

### Output

`-f, --format=FORMAT`
: Output file format

  **required**

  Possible values : `eve`, `jbsq`, `malody`, `memon:legacy`, `memon:v0.1.0`,
  `memon:v0.2.0`, `memon:v0.3.0`, `memon:v1.0.0`, `mono-column`, `memo`,
  `memo1`, `memo2`, `yubiosi:v1.0`, `yubiosi:v1.5`, `yubiosi:v2.0`

`--circlefree`
: Add `#circlefree=1` to the file header when converting to a format from the
  jubeat analyser family.
  
  This means long note ends will be written down with non-circled numbers, like
  in this example :

  ```{code}
  :class: japanese-monospaced
  □□□□　|－－①－|
  ①――＜　|－－②－|
  □□□□　|－－－－|
  □□□□　|－－－－|
  
  □□□□
  ２□□□
  □□□□
  □□□□
  ```

### Other

`--help`
: Show the help text and exit