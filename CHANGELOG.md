# Changelog

A short, human-friendly (but technical !) description of each change made in
each version

Format based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

For version numbers I try to follow [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## v2.1.1
### Fixed
- Attempting to guess the file format of a non UTF-16 file would crash the
  yubiosi 2.0 format guesser, not anymore.

## v2.1.0
### Added
- [yubiosi] 🎉 initial yubiosi support !
### Fixed
- "Unsupported output format" CLI error message would show the *input* format

## v2.0.10
### Fixed
- [memon] Fixed a bug that caused the loader to move the common timing into the
  song timing object

## v2.0.9
### Fixed
- [memon] + [malody] Force utf-8 encoding when reading json files to avoid
  several related problems on Windows :
  - Spurious "Unrecognized file format" error when converting a memon or malody
    file
  - `UnicodeError` when reading a memon or malody file

## v2.0.8
### Fixed
- [memon:v1.0.0] Update dependency `marshmallow-dataclass` to `8.5.14` to fix
  a `TypeError` in a sub-dependency

## v2.0.7
### Changed
- [jubeat-analyser] Unknown commands now only result in a warning being shown
  and don't prevent the file from being parsed

## v2.0.6
### Changed
- [jbsq] Limit the density graph to values between 0 and 8 (previously we only
  restricted it to  0 → 15 but values greater than 8 results in an empty bar in
  jubeat plus). (contributed by [kis](https://gitlab.com/_kis))

## v2.0.5
### Changed
- Exceptions thrown by TimeMap methods now display seconds in decimal form
  and beats in mixed number notation (reported by [kis](https://gitlab.com/_kis))
- Improve some error messages

## v2.0.4
### Fixed
- [konami] Files with duplicate TEMPO lines would raise an expection upon being
  loaded. Now only the last TEMPO line is kept for a given tick, and the
  exception has been changed to a warning with a clearer message (reported by
  [kis](https://gitlab.com/_kis))

## v2.0.3
### Fixed
- [jbsq] Use `IJSQ` as magic bytes when dumping charts with hold notes so that
  the hold symbol shows up properly on a song's preview pane when the song is
  downloaded from the Gift Store or transferred via local matching
  (contributed by [kis](https://gitlab.com/_kis))

## v2.0.2
### Fixed
- Bump the required versions of
  [parsimonious](https://github.com/erikrose/parsimonious) and
  [construct-typing](https://github.com/timrid/construct-typing) to fix
  compatibility bugs with python 3.11

## v2.0.1
### Fixed
- `guess_format()` would raise an exception when called on a specially crafted
  empty jbsq file, not anymore !

## v2.0.0
### BREAKING CHANGE
- Minimum required Python version is now 3.9
### Added
- 🎉 jubeatools finally has proper online docs on readthedocs.io ! 🎉
  read them [here](https://jubeatools.readthedocs.io)
### Changed
- [memo2] The parser now displays much friendlier error messages when
  it finds uneven byte lengths in `#bpp=2` mode
### Fixed
- A fresh install of jubeatools would fail because of a API break in a
  transitive dependecy of `marshmallow-dataclass`, fixed by pinning
  `typing-inspect` to `0.7.1`
- Most loaders would incorrectly use the internal enum value name as difficulty
  names for charts (like `Difficulty.EXTREME`) instead of the regular "display"
  name (like `EXT`), not anymore !
- [eve] + [jbsq]
  - Custom hakus were not taken into account when computing the time of the last
    event, not anymore !
- [jubeat-analyser] Update the repository URL dumped in the comment at the top
  of the file

## v1.4.0
### Added
- [memon]
  - 🎉 inital support for v1.0.0 !
  - `--merge` option allows for several memon files to be merged when
    jubeatools is called on a folder
- Jubeatools can now handle HAKUs in the following formats :
  - [memon:v1.0.0]
  - [eve]
  - [jbsq]
### Changed
- Improved the merging procedure for song objects
- Re-enable calling the CLI on a folder, this was disabled for some reason ?
- The song class now uses a regular dict to map difficuty names to chart
  objects, dissalowing files with duplicate difficulties (`memon:legacy` was the
  only format that *technically* supported this anyway, I conscider it an edge
  case not really worth handling)

## v1.3.0
### Added
- [memon] 🎉 v0.3.0 support

## v1.2.3
### Fixed
- Loaders and Dumpers would recieve options with unwanted default values when
  their corresponding flags were not passed to the commandline, resulting
  in weird bugs, not anymore ! #17

## v1.2.2
### Changed
- Slashes in filenames are now ignored
### Fixed
- Bug when using braces in output filenames
- [malody] Dumping does not write placeholder `null` values anymore

## v1.2.1
### Fixed
- [malody] Parsing a file with keys that are unused for conversion
  (like `meta.mode_ext` or `extra`) would fire errors, not anymore !

## v1.2.0
### Added
- [malody] 🎉 initial malody support !

## v1.1.3
### Fixed
- [jubeat-analyser] All files are read and written in `surrogateescape` error
  mode to mimick the way jubeat analyser handles files at the byte level, without
  caring about whether the whole file can be properly decoded as shift-jis or not
  (Thanks Nomlas and Mintice for noticing this !)

## v1.1.2
### Fixed
- [jubeat-analyser]
    - Accept U+3000 (Ideographic space) as valid whitespace
    - [memo2]
        - Accept `t=` commands anywhere in the file
        - Accept `b=4` (and only 4) anywhere in the file

## v1.1.1
### Fixed
- `construct-typing` is now required for all builds

## v1.1.0
### Added
- [jbsq] 🎉 initial .jbsq support !

## v1.0.1
### Fixed
- Remove debug `print(locals())` mistakenly left in

## v1.0.0
### Added
- [eve]
    - 🎉 .eve support !
    - Add `--beat-snap={number}` loader option to allow aggressive rounding
- Loaders can now take in arguments
### Fixed
- Fix infinite loop that would occur when choosing a deduplicated filename
- [jubeat-analyser] Prettier rendering of decimal values

## v0.2.0
### Added
- [mono-column] #circlefree mode accepts non-16ths notes and falls back to normal symbols when needed
### Fixed
- [jubeat-analyser]
    - Raise exception earlier when a mono-column file is detected by the other #memo parsers (based on "--" separator lines)
    - [memo] [memo1]
        - Fix incorrect handling of mid-chart `t=` and `b=` commands
        - Prettify rendering by adding more blank lines between sections
    - [memo1] Fix dumping of chart with bpm changes happening on beat times that aren't multiples of 1/4
    - [memo2]
        - Fix parsing of BPM changes
        - Fix dumping of BPM changes
- [memon]
    - Fix handling of paths-type values in metadata
    - Fix handling of charts with decimal level value

## v0.1.3
### Changed
- [jubeat-analyser] Use "EXT" instead of "?" as the fallback difficulty name when loading
### Fixed
- [memon] Fix TypeError that would occur when trying to convert
- [memo2] Fix rendering missing blank lines between blocks, while technically still valid files, this made files rendered by jubeatools absolutely fugly and very NOT human friendly

## v0.1.2
### Fixed
- [jubeat-analyser]
    - Fix decimal -> fraction conversion to correctly handle numbers with only 3 decimal places #1
    - Remove Vs from the allowed extra symbols lists as it would clash with long note arrows

## v0.1.1
### Fixed
- [memo2] Loading a file that did not specify any offset (neither by `o=...`, `r=...` nor `[...]` commands) would trigger a TypeError, not anymore ! Offset now defaults to zero.

## v0.1.0
- Initial Release